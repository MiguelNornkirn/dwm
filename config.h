/* See LICENSE file for copyright and license details. */

// Converts N into a tag mask.
// e.g. TAG(2) expands to the mask of the second tag.
// For more details, see https://dwm.suckless.org/customisation/tagmask/ .
#define TAG(n) 1 << (n - 1)

/* If borders should be disabled whenever there's only 1 window. */
#define NO_BORDER_1_CLIENT 1
/*
    Basically, this makes so gaps are always enabled even if
    NO_BORDER_1_CLIENT is 1, but only IF the current client is a terminal
*/
#define ALWAYS_USE_GAPS_FOR_TERMINALS 1

/* Comment to disable */
#define CENTERED_WINDOW_NAMES

/* appearance */
static const unsigned int borderpx  = 2;        /* border pixel of windows */
static const unsigned int snap      = 32;       /* snap pixel */
static const int swallowfloating    = 0;        /* 1 means swallow floating windows by default */
static const int showbar            = 1;        /* 0 means no bar */
static const int topbar             = 1;        /* 0 means bottom bar */
static const char *fonts[]          = { "ZedMonoNerdFont:size=11",
                                        "Noto Serif CJK JP:size=10",
                                        "OpenMoji Color:size=10"};
#define THEME_CAMP_BG
#include "colors.h"

static const unsigned int baralpha = 0xff;
static const unsigned int borderalpha = OPAQUE;
static const char *colors[][3]      = {
	[SchemeNorm] = { col_norm_fg, col_norm_bg, col_norm_border },
	[SchemeSel]  = { col_sel_fg, col_sel_bg,  col_sel_border },
}; static const unsigned int alphas[][3] = { [SchemeNorm] = { OPAQUE, baralpha, borderalpha},
    [SchemeSel]  = { OPAQUE, baralpha, borderalpha},
};

static const unsigned int gappx = 8; /* gap pixel between windows */
static const int CORNER_RADIUS  = 10;

/* tagging */
static const char *tags[] = { "1", "2", "3", "Q", "W", "8", "9"};

static const Rule rules[] = {
    /* xprop(1):
     *	WM_CLASS(STRING) = instance, class
     *	WM_NAME(STRING) = title
     */
    /* class             instance      title     tags mask  isfloating  isterminal  noswallow  monitor */ 
    { "firefox",           NULL,       NULL,       TAG(2),       0,           0,       1,         -1 },
    { "discord",           NULL,       NULL,       TAG(3),       0,           0,       0,         -1 },
    { "obsidian",          NULL,       NULL,       TAG(7),       0,           0,       0,         -1 },
    { "dunst",             NULL,       NULL,       0,            1,           0,       0,         -1 },
    { "win7",              NULL,       NULL,       0,            1,           0,       0,         -1 },
    { "rofi",              NULL,       NULL,       0,            1,           0,       0,         -1 },
    { NULL,                NULL, "Volume Control", 0,            1,           0,       0,         -1 },
    { "st",                NULL,       NULL,       0,            0,           1,       0,         -1 },
    { "Alacritty",         NULL,       NULL,       0,            0,           1,       0,         -1 },
    { "terminator",        NULL,       NULL,       0,            0,           1,       0,         -1 },
    { NULL,                NULL,  "Event Tester",  0,            0,           0,       1,         -1 }, // xev
    { "Jump Dive Clock",   NULL,       NULL,       0,            0,           0,       1,         -1 },
    { "flashplayer 11.exe",NULL,       NULL,       0,            1,           0,       1,         -1 },
    { NULL,                NULL,"Adobe Flash Player 10", 0,      0,           0,       1,         -1 },
    { "surf",              NULL,       NULL,       0,            0,           0,       1,         -1 },
    { NULL,                NULL,    "Debug",       0,            0,           0,       1,         -1 },
    { NULL,                NULL,       "tk",       0,            1,           0,       1,         -1 },
};

/* layout(s) */
static const float mfact     = 0.50; /* factor of master area size [0.05..0.95] */
static const int nmaster     = 1;    /* number of clients in master area */
static const int resizehints = 1;    /* 1 means respect size hints in tiled resizals */
static const int lockfullscreen = 1; /* 1 will force focus on the fullscreen window */

#include "fibonacci.c"
static const Layout layouts[] = {
	/* symbol     arrange function */
        /* first entry is default */
	{ "[]=",      tile },    
	{ "><>",      NULL },    /* no layout function means floating behavior */
	{ "[M]",      monocle },
 	{ "[@]",      spiral },
 	{ "[\\]",      dwindle },
};

/* key definitions */
#define SUPER_KEY Mod4Mask
#define LEFT_ALT Mod1Mask
#define MODKEY SUPER_KEY
#define TAGKEYS(KEY,TAG) \
	{ MODKEY,                       KEY,      view,           {.ui = 1 << TAG} }, \
	{ MODKEY|ControlMask,           KEY,      toggleview,     {.ui = 1 << TAG} }, \
	{ MODKEY|ShiftMask,             KEY,      tag,            {.ui = 1 << TAG} }, \
	{ MODKEY|ControlMask|ShiftMask, KEY,      toggletag,      {.ui = 1 << TAG} },

/* helper for spawning shell commands in the pre dwm-5.0 fashion */
#define SHCMD(cmd) { .v = (const char*[]){ "/bin/sh", "-c", cmd, NULL } }

/* commands */
static char dmenumon[2] = "0"; /* component of dmenucmd, manipulated in spawn() */
static const char *dmenucmd[] = { "dmenu_run", "-m", dmenumon, NULL };
static const char *aplicationrunnercmd[] = { "./script-utils/dmenu-launch.py", NULL };
static const char *pkillpromptcmd[] = { "./script-utils/pkill.sh", NULL };
static const char *termcmd[]  = { "st", NULL };
static const char *fileexplorercmd[] = { "pcmanfm", NULL };
static const char *webbrowsercmd[] = { "firefox", NULL };
static const char *screenshotcmd[] = { "flameshot", "gui", NULL };
static const char *volumemixercmd[] = { "pavucontrol", NULL };
static const char scratchpadname[] = "scratchpad";
static const char *scratchpadcmd[] = { "st", "-t", scratchpadname, "-g", "120x34", "-e", "nano", "/tmp/scratchpad", NULL };

#include "movestack.c"
static Key keys[] = {
	/* modifier                     key        function        argument */
        { MODKEY|ShiftMask,             XK_f,      togglefullscr,  {0} },
	{ MODKEY|ShiftMask,             XK_j,      movestack,      {.i = +1 } },
	{ MODKEY|ShiftMask,             XK_k,      movestack,      {.i = -1 } },
	{ MODKEY,                       XK_x,  togglescratch,  {.v = scratchpadcmd } },
        { MODKEY,                       XK_i,      spawn,          SHCMD("imv $HOME/coisas/Jesus.jpg")},
        { MODKEY,                       XK_z,      spawn,          {.v = aplicationrunnercmd } },
	{ MODKEY,                       XK_Return, spawn,          {.v = termcmd } },
	{ MODKEY,                       XK_e,      spawn,          {.v = fileexplorercmd } }, 
	/* { MODKEY,                       XK_b,      spawn,          {.v = webbrowsercmd } }, */
	{ MODKEY,                       XK_p,      spawn,          {.v = screenshotcmd } },
	{ MODKEY,                       XK_a,      spawn,          {.v = volumemixercmd } },
	{ MODKEY|ShiftMask,             XK_p,      spawn,          {.v = pkillpromptcmd } },
	{ MODKEY,                       XK_g,      spawn,          SHCMD("$HOME/script-utils/gaming-mode.sh") },
	{ MODKEY|ShiftMask,             XK_b,      togglebar,      {0} },
	{ MODKEY,                       XK_j,      focusstack,     {.i = +1 } },
	{ MODKEY,                       XK_k,      focusstack,     {.i = -1 } },
	{ MODKEY|ShiftMask,             XK_s,      incnmaster,     {.i = +1 } },
	{ MODKEY|ShiftMask,             XK_x,      incnmaster,     {.i = -1 } },
	{ MODKEY,                       XK_h,      setmfact,       {.f = -0.05} },
	{ MODKEY,                       XK_l,      setmfact,       {.f = +0.05} },
	{ MODKEY|ShiftMask,             XK_Return, zoom,           {0} },
	{ MODKEY,                       XK_Tab,    view,           {0} },
	{ MODKEY|ShiftMask,             XK_c,      killclient,     {0} },
	{ MODKEY,                       XK_t,      setlayout,      {.v = &layouts[0]} },
	{ MODKEY,                       XK_f,      setlayout,      {.v = &layouts[1]} },
	{ MODKEY,                       XK_m,      setlayout,      {.v = &layouts[2]} },
	{ MODKEY,                       XK_r,      setlayout,      {.v = &layouts[3]} },
	{ MODKEY|ShiftMask,             XK_r,      setlayout,      {.v = &layouts[4]} },
	{ MODKEY,                       XK_space,  setlayout,      {0} },
	{ MODKEY|ShiftMask,             XK_space,  togglefloating, {0} },
	{ MODKEY,                       XK_0,      view,           {.ui = ~0 } },
	{ MODKEY|ShiftMask,             XK_0,      tag,            {.ui = ~0 } },
	{ MODKEY,                       XK_comma,  focusmon,       {.i = -1 } },
	{ MODKEY,                       XK_period, focusmon,       {.i = +1 } },
	{ MODKEY|ShiftMask,             XK_comma,  tagmon,         {.i = -1 } },
	{ MODKEY|ShiftMask,             XK_period, tagmon,         {.i = +1 } },
	TAGKEYS(                        XK_1,                      0)
	TAGKEYS(                        XK_2,                      1)
	TAGKEYS(                        XK_3,                      2)
	TAGKEYS(                        XK_q,                      3)
	TAGKEYS(                        XK_w,                      4)
	TAGKEYS(                        XK_8,                      5)
	TAGKEYS(                        XK_9,                      6)
	{ MODKEY|ShiftMask,             XK_Delete, quit,           {0} },
	{ MODKEY|ControlMask|ShiftMask, XK_q,      quit,           {1} }, 
};

/* button definitions */
/* click can be ClkTagBar, ClkLtSymbol, ClkStatusText, ClkWinTitle, ClkClientWin, or ClkRootWin */
static Button buttons[] = {
	/* click                event mask      button          function        argument */
	{ ClkLtSymbol,          0,              Button1,        setlayout,      {0} },
	{ ClkLtSymbol,          0,              Button3,        setlayout,      {.v = &layouts[2]} },
	{ ClkWinTitle,          0,              Button2,        zoom,           {0} },
	{ ClkStatusText,        0,              Button2,        spawn,          {.v = termcmd } },
	{ ClkClientWin,         MODKEY,         Button1,        movemouse,      {0} },
	{ ClkClientWin,         MODKEY,         Button2,        togglefloating, {0} },
	{ ClkClientWin,         MODKEY,         Button3,        resizemouse,    {0} },
	{ ClkTagBar,            0,              Button1,        view,           {0} },
	{ ClkTagBar,            0,              Button3,        toggleview,     {0} },
	{ ClkTagBar,            MODKEY,         Button1,        tag,            {0} },
	{ ClkTagBar,            MODKEY,         Button3,        toggletag,      {0} },
};


