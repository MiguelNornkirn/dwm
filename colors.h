/* TODO: add https://github.com/drewtempelmeyer/palenight.vim */
#ifdef THEME_CAMP_BG
static const char col_norm_fg[]     = "#ebdbb2";
static const char col_norm_bg[]     = "#111111";
static const char col_norm_border[] = "#3c3836";
static const char col_sel_fg[]      = "#fbf1c7";
static const char col_sel_bg[]      = "#3d5754"; /* Top bar selection */
static const char col_sel_border[]  = "#697762";
#endif
