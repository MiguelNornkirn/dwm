My personal fork of dwm 6.3. 

## Customization list

### Handmade or tweaked
* Gaps can be optionally removed when there's only one window on the screen;
* theme toggler see [colors.h](colors.h);
* terminals can be optionally configured to always have gaps (depends on the terminal swallowing patch);
* titles can be optionally centered at the middle of the title bar;
* autostart scripts in ~/.config/startup-scripts/autostart.sh and
  ~/.config/startup-scripts/autostart_blocking.sh.

Configuration for these can be seen in config.h.


### Patches
* [gaps](https://dwm.suckless.org/patches/uselessgap/);
* [actualfullscreen](https://dwm.suckless.org/patches/actualfullscreen/).
* [titles are centered at the middle of the bar](https://dwm.suckless.org/patches/centeredwindowname/);
* [scratchpad using st and nano](https://dwm.suckless.org/patches/scratchpad/);
* [terminal swallowing](https://dwm.suckless.org/patches/swallow/);
* [fibonacci layouts](https://dwm.suckless.org/patches/fibonacci/);
* [restartsig](https://dwm.suckless.org/patches/restartsig/);
* [...].

There are other patches that I don't remember their names (sorry, forgot to
document).

## Status bar

My status bar is just a script that uses `xsetroot -name "foo"`
to add information to the default DWM status bar.

## Original README
```
dwm - dynamic window manager
============================
dwm is an extremely fast, small, and dynamic window manager for X.


Requirements
------------
In order to build dwm you need the Xlib header files.


Installation
------------
Edit config.mk to match your local setup (dwm is installed into
the /usr/local namespace by default).

Afterwards enter the following command to build and install dwm (if
necessary as root):

    make clean install


Running dwm
-----------
Add the following line to your .xinitrc to start dwm using startx:

    exec dwm

In order to connect dwm to a specific display, make sure that
the DISPLAY environment variable is set correctly, e.g.:

    DISPLAY=foo.bar:1 exec dwm

(This will start dwm on display :1 of the host foo.bar.)

In order to display status info in the bar, you can do something
like this in your .xinitrc:

    while xsetroot -name "`date` `uptime | sed 's/.*,//'`"
    do
    	sleep 1
    done &
    exec dwm


Configuration
-------------
The configuration of dwm is done by creating a custom config.h
and (re)compiling the source code.
